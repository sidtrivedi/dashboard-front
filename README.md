# Dashboard Application

A dashboard application for a retail business that runs a number of department stores across the
Canada. Each department store purchases the certain products from manufactures across the
Canada and sells to the local customers. Every store invests its revenue in essential aspects to
operate the store such as human resource, electricity, hydro, furniture, technology,
advertisement and many such services. Human Resource plays a key role in organization’s
operations thus each store hires employees for different positions.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them
In order to install the application you will need node and npm installed on your machine.

### Installing

A step by step series of examples that tell you have to get a development env running

Say what the step will be

```
npm install
```

And repeat

```
npm run-dev
```

End with an example of getting some data out of the system or using it for a little demo

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [ReactJS](https://reactjs.org/docs/hello-world.html) - The web framework used
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds
 

## Authors

* **Siddharth Trivedi** - *Initial work* - [sidtrivedi](https://gitlab.com/sidtrivedi)